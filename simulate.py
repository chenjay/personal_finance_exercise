import csv
import numpy
import matplotlib.pyplot as plt
import pprint
import random

from collections import namedtuple

pp = pprint.PrettyPrinter(indent=4)

StockMarketReturns = namedtuple('StockMarketReturns', 'month percentReturn')

RENT_PER_MONTH = 2000
params = [ {
    'post_tax_rate': 1.0 - 0.35,
    'salary_per_month': 100000.0 / 12.0,
    'cost_per_month': 1000.0 + RENT_PER_MONTH,
    'starting_net_worth': 10000.0,
    'financial_goal': 5000000.0 # 200,000 per year investment income (at 4% / year)
}, {
    'post_tax_rate': 1.0 - 0.35,
    'salary_per_month': 100000.0 / 12.0,
    'cost_per_month': 2000.0 + RENT_PER_MONTH,
    'starting_net_worth': 10000.0,
    'financial_goal': 5000000.0 # 200,000 per year investment income (at 4% / year)
}, {
    'post_tax_rate': 1.0 - 0.35,
    'salary_per_month': 100000.0 / 12.0,
    'cost_per_month': 3000.0 + RENT_PER_MONTH,
    'starting_net_worth': 10000.0,
    'financial_goal': 5000000.0 # 200,000 per year investment income (at 4% / year)
} ]

stock_market_returns_per_month = []
with open('./data/sp500.csv', 'r') as csvfile:
    reader = csv.reader(csvfile)
    # ['Date', 'Open', 'High', 'Low', 'Close', 'Volume', 'Adj Close']
    header = reader.next()
    for row in reader:
        stock_market_returns_per_month.append(
            StockMarketReturns(
                row[0],
                float(row[6]) / float(row[1]) - 1.0))

def simulate(params):
    starting_net_worth = params['starting_net_worth']
    financial_goal = params['financial_goal']
    salary_per_month = params['salary_per_month']
    post_tax_rate = params['post_tax_rate']
    cost_per_month = params['cost_per_month']
    samples = []
    for i in range(0, 10000):
        net_worth = starting_net_worth
        num_months = 0
        while net_worth < financial_goal:
            num_months += 1
            net_worth += salary_per_month * post_tax_rate - cost_per_month

            stock_movement = stock_market_returns_per_month[random.randint(0, len(stock_market_returns_per_month) - 1)]
            net_worth *= (1.0 + stock_movement.percentReturn)
        samples.append(num_months / 12.0)
    print numpy.mean(samples), '\n', numpy.std(samples), '\n'

    # Uncomment the below lines to plot the histogram. Note the next param won't run until you close the plot.
    # plt.hist(samples)
    # plt.show()

for param in params:
    simulate(param)
