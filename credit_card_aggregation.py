import csv
import datetime
import numpy
import pprint
import os

from collections import namedtuple

pp = pprint.PrettyPrinter(indent=4)

Transaction = namedtuple('Transaction', 'date cost')

transactions = []
for filename in os.listdir('./data/chase'):
    with open('./data/chase/' + filename, 'r') as csvfile:
        reader = csv.reader(csvfile)
        # ['Type', 'Trans Date', 'Post Date', 'Description', 'Amount']
        header = reader.next()
        for row in reader:
            if row[0] == 'Sale':
                d = datetime.datetime.strptime(row[1], '%m/%d/%Y')
                transactions.append(Transaction(d, abs(float(row[len(row) - 1]))))

for filename in os.listdir('./data/capital_one'):
    with open('./data/capital_one/' + filename, 'r') as csvfile:
        reader = csv.reader(csvfile)
        # ['Transaction Date', 'Posted Date', 'Card No.', 'Description', 'Category', 'Debit', 'Credit']
        # ['Stage', ' Transaction Date', ' Posted Date', ' Card No.', ' Description', ' Category', ' Debit', ' Credit']
        header = reader.next()
        if header[0] == 'Transaction Date':
            date_index = 0
        else:
            date_index = 1
        for row in reader:
            if date_index == 0:
                d = datetime.datetime.strptime(row[date_index], '%Y-%m-%d')
            else:
                d = datetime.datetime.strptime(row[date_index], '%m/%d/%Y')
            debit = row[len(row) - 2]
            if len(debit):
                transactions.append(Transaction(d, abs(float(debit))))

cost_per_month = {}
cost_per_year = {}
for transaction in transactions:
    month = str(transaction.date.month)
    if len(month) == 1:
        month = '0' + month
    date_key = str(transaction.date.year) + '/' + month
    if date_key in cost_per_month:
        cost_per_month[date_key] += transaction.cost
    else:
        cost_per_month[date_key] = transaction.cost

for key in sorted(cost_per_month):
    year = key.split('/')[0]
    if year in cost_per_year:
        cost_per_year[year] += cost_per_month[key]
    else:
        cost_per_year[year] = cost_per_month[key]
    print key, cost_per_month[key]

print 'Cost per month'
pp.pprint(cost_per_month)

print 'Cost per year'
pp.pprint(cost_per_year)
