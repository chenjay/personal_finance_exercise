## Install

On OSX:

> brew install numpy

> pip install matplotlib

## Simulations on how long to reach financial goal

Add or change the parameters you wish to simulate at the top of simulate.py. After that:

> python simulate.py

If you want to plot the distributions, uncomment the plot lines in `simulate.py`

## Basic credit card aggregations

Download your Chase / Capital One transactions online and throw them into './data/chase' or './data/capital_one' respectively.
After that:

> python credit_card_aggregation.py

To get basic monthly / yearly aggregations of your spendings
